FROM node:14 as builder

# ENV PROJECT_ENV production
# ENV NODE_ENV production
WORKDIR /code

ADD package.json /code
RUN npm config set registry https://r.npm.taobao.org/
RUN npm get registry
RUN npm install

ADD . /code
RUN npm run build

# 选择更小体积的基础镜像
FROM nginx:alpine
COPY --from=builder /code/dist /usr/share/nginx/html
COPY --from=builder /code/nginx.conf /etc/nginx/conf.d/aListen80.conf
