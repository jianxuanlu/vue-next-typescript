import content from './package.json';
export type Script = keyof typeof content.scripts;
