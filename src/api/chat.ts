import { Messages } from '@/types/messages';
import axios from 'axios';
import { UploaderFileListItem } from 'vant';

export const getAllMessages = () => {
  return axios.get<Messages>('/api/message');
};

export const uploadFile = (
  file: UploaderFileListItem,
  compressedFile?: File
) => {
  const formData = new FormData();
  formData.append('file', file.file || '');
  if (compressedFile !== undefined) {
    formData.append('compressedFile', compressedFile);
  }
  return axios.post('/api/file', formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
  });
};
