export type File = {
  type: 'audio' | 'image' | 'video';
  fileName: string;
  noCompressValue?: string;
};
export type Text = {
  type: 'text';
};
export type MessageWithoutId = {
  value: string;
  date: string;
  nickName: string;
} & (File | Text);

export type Message = MessageWithoutId & { _id: string };
export type ChangeOriginalFile = (id: string) => void;
export type ClickDelete = (id: string) => void;
export type ClickOriginal = () => void;
export type ClickCompressed = () => void;
export type LoadedmetadataVideo = () => void;
