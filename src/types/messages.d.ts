import { Message } from './message';

export type Messages = Message[];
export type ScrollToBottom = () => void;
export type ExportScrollToBottom = (scrollToBottom: ScrollToBottom) => void;
