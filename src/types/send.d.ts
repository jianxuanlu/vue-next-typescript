export type Send = (text: string) => void;
export type Focus = () => void;
