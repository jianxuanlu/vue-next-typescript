import { Message } from './message';

export type DeleteMessage = (id: string) => void;
export type AddMessage = (message: Message) => void;
export type ScrollToBottom = () => void;
export type ClickDelete = () => void;
