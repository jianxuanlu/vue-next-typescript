export const awaitWrap = <T>(promise: Promise<T>) => {
  return promise
    .then<[T, null]>((data: T) => [data, null])
    .catch<[null, unknown]>((err) => [null, err]);
};
