import Compressor from 'compressorjs';
export const compressImage = (file: File): Promise<File> => {
  return new Promise((resolve, reject) => {
    new Compressor(file, {
      // todo 这个参数不能压缩到1M左右，奇怪
      quality: 1000000 / file.size,
      success: (res) => {
        resolve(new File([res], file.name, { type: file.type }));
      },
      error: (error) => reject(error),
    });
  });
};
