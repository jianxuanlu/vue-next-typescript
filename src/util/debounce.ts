// eslint-disable-next-line @typescript-eslint/ban-types
export const debounce = (fn: Function, timeout: number, immediate = false) => {
  let timer: number | undefined;
  let hasRunFn = false;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const newFn = function (this: any, ...args: any[]) {
    if (!hasRunFn && immediate) {
      fn.apply(this, args);
      fn(...args);
      hasRunFn = true;
      return;
    }
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(this, args);
    }, timeout);
  };
  return newFn;
};
