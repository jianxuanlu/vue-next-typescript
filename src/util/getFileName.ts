export const getFileName = (path: string) => {
  const index = path.lastIndexOf('/');
  return path.slice(index + 1, path.length);
};
