git pull
project='vue-next-typescript'
docker stop $project
docker rm $project
docker rmi $project
docker build -t $project .
docker run --name=$project -d -p 4000:80 $project
