"use strict";
exports.__esModule = true;
// eslint-disable-next-line @typescript-eslint/no-var-requires
var webpack = require('webpack');
var script = process.env.npm_lifecycle_event;
var config_1 = require("./config");
var config = {
    devServer: {
        proxy: {
            '/api': {
                target: script === 'serve' ? 'http://localhost:3000' : "http://" + config_1.ip + ":5000"
            }
        }
    },
    configureWebpack: function (config) {
        config.plugins.push(new webpack.DefinePlugin({
            script: "'" + script + "'"
        }));
    }
};
module.exports = config;
