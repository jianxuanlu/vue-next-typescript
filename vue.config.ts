/**
 * this file is for generating vue.config.js
 */
import { ProjectOptions } from '@vue/cli-service/types/ProjectOptions';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const webpack = require('webpack');

import { Script } from './script';
const script = process.env.npm_lifecycle_event as Script;

import { ip } from './config';
const config: ProjectOptions = {
  devServer: {
    proxy: {
      '/api': {
        target:
          script === 'serve' ? 'http://localhost:3000' : `http://${ip}:5000`,
        // `http://${ip}:5000/`,
      },
    },
  },
  configureWebpack: (config) => {
    config.plugins.push(
      new webpack.DefinePlugin({
        script: `'${script}'`,
      })
    );
  },
};
module.exports = config;
